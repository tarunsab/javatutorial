package com.tarsab.Car;

public class Basic3Series extends PartialCar {

  public Basic3Series(int seats, String colour, double engineSize, String numberplate) {
    super(seats, colour, engineSize, numberplate);
  }

  @Override
  void unlock() {
    System.out.println("Keyless unlocking");
  }

}
