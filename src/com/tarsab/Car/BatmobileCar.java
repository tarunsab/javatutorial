package com.tarsab.Car;

public class BatmobileCar implements Car{

  @Override
  public int getSeats() {
    return 2;
  }

  @Override
  public String getColor() {
    return "Black";
  }

  @Override
  public void reverse() {
    System.out.println("VROOOOM");
  }

  @Override
  public void unlock() {
    System.out.println("knows when batman is close");
  }

}
