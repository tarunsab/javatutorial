package com.tarsab.Car;

public class SECorsa extends BasicCorsa {

  private boolean hasFrontSensors = false;

  public SECorsa(int seats, String colour, double engineSize, String numberplate,
                 boolean hasFrontSensors) {
    super(seats, colour, engineSize, numberplate);
    this.hasFrontSensors = hasFrontSensors;
  }

  public boolean hasFrontSensors() {
    return hasFrontSensors;
  }

  public static boolean hasBackSensors() {
    return true;
  }

  @Override
  public void reverse() {
    System.out.println("Reversing, beep beep beep");
  }

}
