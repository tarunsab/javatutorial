package com.tarsab.Car;

abstract public class PartialCar {

  private int seats;
  private String colour;
  private double engineSize;
  private String numberplate;

  public PartialCar(int seats, String colour, double engineSize, String numberplate) {
    this.seats = seats;
    this.colour = colour;
    this.engineSize = engineSize;
    this.numberplate = numberplate;
  }

  public int getSeats() {
    return seats;
  }

  public String getColour() {
    return colour;
  }

  public double getEngineSize() {
    return engineSize;
  }

  public String getNumberplate() {
    return numberplate;
  }

  public void reverse() {
    System.out.println("Reversing");
  }

  abstract void unlock();

}
