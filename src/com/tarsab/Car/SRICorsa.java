package com.tarsab.Car;

public class SRICorsa extends BasicCorsa {

  private boolean hasHeatedSeats = false;

  public SRICorsa(int seats, String colour, double engineSize, String numberplate,
                  boolean hasHeatedSeats) {
    super(seats, colour, engineSize, numberplate);
    this.hasHeatedSeats = hasHeatedSeats;
  }

  public boolean hasHeatedSeats() {
    return hasHeatedSeats;
  }

}
