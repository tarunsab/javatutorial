package com.tarsab.Car;

public class Program {

  public static void main(String[] args) {

    /*
                    PartialCar
                        ^
           ___________      __________
          |Basic Corsa|    | 3 series |
          |___________|    |__________|
           _____^_______                   ^ = Extends
           |           |
       _________     ________
      |SRI Corsa|   |SE Corsa|
      |_________|   |________|

     */


    BasicCorsa basicCorsa = new BasicCorsa(4, "Silver", 1.6, "HS04GLY");
    basicCorsa.getColour();
//    basicCorsa.reverse();


    SRICorsa sriCorsa = new SRICorsa(4, "White", 1.4, "ND12OVW", true);
    sriCorsa.getColour();
    sriCorsa.hasHeatedSeats();
//    sriCorsa.reverse();

    SECorsa seCorsa = new SECorsa(4, "Red", 1.4, "KT13OHC", false);
    seCorsa.getColour();
    seCorsa.hasFrontSensors();
    seCorsa.hasBackSensors();
//    seCorsa.reverse();

    PartialCar car = seCorsa;
    car.reverse();

    //Casting
    BasicCorsa basic = seCorsa;
    ((SECorsa) basic).hasFrontSensors();

  }

}


