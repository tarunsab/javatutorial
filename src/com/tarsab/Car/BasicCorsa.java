package com.tarsab.Car;

public class BasicCorsa extends PartialCar {

  public BasicCorsa(int seats, String colour, double engineSize, String numberplate) {
    super(seats, colour, engineSize, numberplate);
  }

  @Override
  void unlock() {
    System.out.println("Remote Locking");
  }

  //Decorating
  public String getMake() {
    return "Vauxhall";
  }

}
