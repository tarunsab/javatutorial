package com.tarsab.Car;

public interface Car {

  public int getSeats();

  public String getColor();

  public void reverse();

  public void unlock();

}
