package com.tarsab.Dog;

public class Dog {

  private String name = "Sukhvir";
  private String colour = "White";
  private String password = "pass";


  //Constructor
  public Dog(String name, String colour) {
    this.name = name;
    this.colour = colour;
  }

  //Constructor
  public Dog() {
  }

  public static void doesEverything() {
//    run();
//    barks();
  }

  public void run() {
    System.out.println(colour + " " + name + " runs");
  }

  public void barks() {
    System.out.println(colour + " " + name + " barks");
  }

  public String getPassword() {
    return decrypt(password);
  }

  private String encrypt(String password) {
    return password + "123123";
  }

  public void setPassword(String newPassword) {
    this.password = encrypt(newPassword);
  }

  private String decrypt(String password) {
    return password;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Dog dog = (Dog) o;
    if (!name.equals(dog.name)) {
      return false;
    }
    return colour.equals(dog.colour);
  }

  @Override
  public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + colour.hashCode();
    return result;
  }

}
