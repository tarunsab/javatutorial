package com.tarsab.Dog;

public class DobermanDog extends Dog {

  private int size;

  public DobermanDog(String name, String colour, int size) {
    super(name, colour);
    this.size = size;
  }

}
