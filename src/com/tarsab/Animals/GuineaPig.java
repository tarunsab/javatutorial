package com.tarsab.Animals;

public class GuineaPig implements Animal {

  @Override
  public void eats() {
    System.out.println("Eats Guinea Pig nuggets");
  }

  @Override
  public void sleep() {
    System.out.println("Takes a nap several times a day");
  }

  @Override
  public void move() {
    System.out.println("Guinea pig hops");
  }

  @Override
  public void numberOfLegs() {
    System.out.println("Guinea pig has 4 legs");
  }

  @Override
  public void communicates() {
    System.out.println("Guinea pig purrs and sqeaks");
  }

}
