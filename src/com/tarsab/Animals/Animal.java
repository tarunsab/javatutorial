package com.tarsab.Animals;

public interface Animal {
  void eats();  //What the animal eats
  void sleep(); //Time that the animal sleeps for
  void move();  //How the animal moves
  void numberOfLegs();
  void communicates();
}
