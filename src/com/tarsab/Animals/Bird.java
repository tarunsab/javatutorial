package com.tarsab.Animals;

public class Bird implements Animal {

  @Override
  public void eats() {
    System.out.println("Eats bird food");
  }

  @Override
  public void sleep() {
    System.out.println("Bird hibernates");
  }

  @Override
  public void move() {
    System.out.println("Bird flies");
  }

  @Override
  public void numberOfLegs() {
    System.out.println("Bird has 2 legs");
  }

  @Override
  public void communicates() {
    System.out.println("Bird chirps");
  }
}
