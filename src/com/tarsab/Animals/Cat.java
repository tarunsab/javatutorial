package com.tarsab.Animals;

public class Cat implements Animal {

  @Override
  public void eats() {
    System.out.println("Eats cat food");
  }

  @Override
  public void sleep() {
    System.out.println("Cat is napping for 10 mins");
  }

  @Override
  public void move() {
    System.out.println("The cat strolls");
  }

  @Override
  public void numberOfLegs() {
    System.out.println("Cat has 4 legs");
  }

  @Override
  public void communicates() {
    System.out.println("Cat meows");
  }

}
