package com.tarsab.Animals;

import com.tarsab.Dog.TarunList;

import java.util.ArrayList;
import java.util.HashMap;

public class AnimalProgram {

  public static void main(String[] args) {

    Cat minnie = new Cat();
    Bird perry = new Bird();
    GuineaPig oreo = new GuineaPig();
    GuineaPig rosie = new GuineaPig();

    HashMap<AnimalType, Animal> allAnimalDictionary = new HashMap<>();

    allAnimalDictionary.put(AnimalType.CAT, minnie);
    allAnimalDictionary.put(AnimalType.BIRD, perry);
    allAnimalDictionary.put(AnimalType.GUINEAPIG, oreo);

    Animal animal = allAnimalDictionary.get(AnimalType.CAT);
    animal.eats();

    ArrayList<Animal> animalList = new ArrayList<>();
    TarunList<Animal> aList;

    animalList.add(minnie);
    animalList.add(perry);
    animalList.add(oreo);
    animalList.add(rosie);
//    animalList.add(new Dog("tarun", "brown"));

//    for (Animal a : animalList) {
//      a.numberOfLegs();
//    }

  }

}
