package com.tarsab;

import com.tarsab.Dog.Dog;

public class Main {

  public static void main(String[] args) {

    Dog ImperialDog = new Dog();
    ImperialDog.doesEverything();
    Dog.doesEverything();

    Dog UCLDog = new Dog("Tarun", "White");

    if (ImperialDog.equals(UCLDog)) {
      System.out.println("They're not the same");
    }

    String gotPassword = ImperialDog.getPassword();
    System.out.println(gotPassword);

  }

}
