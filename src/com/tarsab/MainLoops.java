package com.tarsab;

import java.util.ArrayList;

public class MainLoops {

  public static void main(String[] args) {

    //IF
    if (5 < 6) {
      System.out.println("5 is less than 6");
    }

    //WHILE
    int i = 9;
    while (i < 10) {
      System.out.println(i + " is less than 10");
      i--;
    }

    //DO WHILE
    int j = 9;
    do {
      System.out.println(i + " is less than 10");
      j--;
    } while (j < 10);

    //FOR
    for (int k = 0; k < 10; k++) {
      System.out.println("k is " + k);
    }

    ////FOREACH
    int[] ints = {1, 2, 3, 4, 5};
    for (Integer k : ints) {
      System.out.println("k is " + k);
      if (k == 5) {
        break;
      }
      if (k == 8) {
        continue;
      }
    }

    Integer tarunNum = 7;
    switch (tarunNum) {
      case 1:
        System.out.println("Its 1");
        break;
      case 2:
        System.out.println("Its 2");
        break;
      case 3:
        System.out.println("Its 3");
        break;
      case 7:
        System.out.println("Its 7");
        break;
      default:
        System.out.println("DEFAULT");
    }

    Integer[][] sponsorsContactedMatrix = {{1,2,3},{2,3,4},{3,4,5}};
    Integer[] sponsorsContactedNew = new Integer[10];

    Integer arrayElem = sponsorsContactedNew[5];

  }

}
